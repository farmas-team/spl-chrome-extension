chrome.browserAction.disable();

const today = moment().startOf('day');

var writeStorage = function (obj) {
  console.log('Writting to storage', obj);
  chrome.storage.sync.set(obj, function () {
    chrome.browserAction.enable();
  });
}

var loadBadge = function (checkedOutSummary) {
  var badgeText = '-';

  if (checkedOutSummary && checkedOutSummary.nextDueDate) {
    const dueDate = moment(checkedOutSummary.nextDueDate);
    const dueDateDiff = dueDate.diff(today, 'days');
    badgeText = Math.abs(dueDateDiff).toString();
  }

  chrome.browserAction.setBadgeText({ text: badgeText });
  chrome.browserAction.setBadgeBackgroundColor({ color: checkedOutSummary.isExpired ? '#ff3333' : '#3385ff' });
}

var requestRemoteData = function (settings) {
  var url = settings.dataUrl;
  var data = {
    splUsername: settings.splUsername,
    splPassword: settings.splPassword
  };
  console.log('Executing request', url);

  var xhr = new XMLHttpRequest();
  xhr.open('POST', url, true);
  xhr.setRequestHeader('Content-Type', 'application/json;charset=UTF-8');
  xhr.setRequestHeader('x-functions-key', settings.dataToken);

  xhr.onreadystatechange = function () {
    if (xhr.readyState == 4) {
      var resp = JSON.parse(xhr.responseText);

      console.log('Loading server data', resp);
      loadBadge(resp.checkedOutSummary);
      writeStorage({
        lastRefresh: moment().format("YYYY-MM-DD"),
        lastRefreshTime: new Date().getTime(),
        checkedOutSummary: resp.checkedOutSummary,
        checkedOutItems: resp.checkedOutItems,
        heldItems: resp.heldItems,
        heldSummary: resp.heldSummary
      });
    }
  }

  xhr.send(JSON.stringify(data));
}

var loadStatus = function () {
  console.log('Polling status', moment().format('YYYY/MM/DD hh:mm'));

  chrome.storage.sync.get(null, function (data) {
    var settings = data && data.settings;
    var lastRefresh = data && data.lastRefresh;
    var lastRefreshTime = data && data.lastRefreshTime && moment(data.lastRefreshTime).format('YYYY/MM/DD hh:mm');

    console.log('Last Refresh', lastRefresh, 'Last Refresh Time', lastRefreshTime);
    console.log('Loaded Storage', data);

    if (!settings) {
      // Extension does not have settings yet.
      console.log('Loading empty data');
      writeStorage({ checkedOutItems: [], heldItems: [] });
      chrome.browserAction.setBadgeText({ text: 'X' });
      chrome.browserAction.setBadgeBackgroundColor({ color: '#ff3333' });

      chrome.browserAction.enable();
    } else if (lastRefresh && lastRefresh == today.format('YYYY-MM-DD')) {
      console.log('Using data in storage', data);
      loadBadge(data.checkedOutSummary);
      chrome.browserAction.enable();
    } else {
      chrome.browserAction.setBadgeText({ text: '?' });
      requestRemoteData(data.settings);
    }
  });
}

chrome.alarms.onAlarm.addListener(() => {
  loadStatus();
});

chrome.alarms.create('splAlarm', {
  delayInMinutes: 60,
  periodInMinutes: 60
});

loadStatus();
