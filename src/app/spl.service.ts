import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { LibraryInfo, Settings } from './app.typings';
import * as toastr from 'toastr';

@Injectable({
  providedIn: 'root'
})
export class SplService {

  constructor(private http: HttpClient) { }

  public getLibraryItems(settings: Settings): Observable<LibraryInfo> {
    if (!settings) {
      return of({ checkedOutItems: [] });
    }

    console.log('Requesting data from server', settings.dataUrl);
    const data = { splUsername: settings.splUsername, splPassword: settings.splPassword };
    const headers = { 'x-functions-key': settings.dataToken || 'not-defined' };

    return this.http.post<LibraryInfo>(settings.dataUrl, data, { headers: headers }).pipe(
      catchError((err: HttpErrorResponse) => {
        const message = `Failed to retrieve library data. Error: ${err.message}`;
        console.log(message);

        toastr.options.timeOut = 0;
        toastr.options.closeButton = true;
        toastr.error(message);
        throw err;
      })
    );
  }
}
