import { ChromeTestService } from './chrome-test.service';
import { ChromeExtensionService } from './chrome-extension.service';
import { Config } from './config';
import { ChromeWebService } from './chrome-web.service';
import { SplService } from './spl.service';
import { ActivatedRoute } from '@angular/router';

export const ChromeServiceFactory = (splService: SplService, route: ActivatedRoute) => {
  if (Config.chromeServiceProvider === 'extension') {
    return new ChromeExtensionService();
  } else if (Config.chromeServiceProvider === 'web') {
    return new ChromeWebService(splService, route);
  } else {
    return new ChromeTestService();
  }
};
