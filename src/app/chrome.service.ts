import { LocalStorage, CheckedOutItem } from './app.typings';
import { Observable } from 'rxjs';

export class ChromeService {

  constructor() { }

  readStorageData(keys: string | string[] | Object | null): Observable<LocalStorage> {
    throw new Error('Not implemented');
  }

  writeStorageData(data: LocalStorage, callback?: () => void): void {
    throw new Error('Not Implemented');
  }

  setBadge(text: string, color: string): void {
    throw new Error('Not Implemented');
  }
}
