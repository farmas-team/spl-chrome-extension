import { Component, OnInit } from '@angular/core';
import { CheckedOutItem, Settings, LibraryInfo, HeldItem } from '../app.typings';
import { ChromeService } from '../chrome.service';
import { Router } from '@angular/router';
import * as moment from 'moment';
import { SplService } from '../spl.service';

const today = moment().startOf('day');

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.less']
})
export class MainComponent implements OnInit {

  public checkedOutItems: CheckedOutItem[] = [];
  public heldItems: HeldItem[] = [];
  public isLoading = true;
  public activeTabIndex = 0;

  private _settings: Settings;
  private _lastRefreshTime: number;

  constructor(private chromeService: ChromeService, private splService: SplService, private router: Router) { }

  ngOnInit() {
    this.chromeService.readStorageData(['checkedOutItems', 'heldItems', 'settings', 'lastRefreshTime']).subscribe(data => {
      if (!data.settings) {
        this.router.navigate(['/settings']);
      } else {
        this._settings = data.settings;
        this._lastRefreshTime = data.lastRefreshTime;
        this.checkedOutItems = data.checkedOutItems;
        this.heldItems = data.heldItems;
        this.isLoading = false;
      }
    });
  }

  public refresh(): void {
    this.isLoading = true;
    this.checkedOutItems = [];

    this.splService.getLibraryItems(this._settings).subscribe(libraryInfo => {
      let badgeText = '-';
      let badgeColor = '#3385ff';

      if (libraryInfo.checkedOutSummary && libraryInfo.checkedOutSummary.nextDueDate) {
        const dueDate = moment(libraryInfo.checkedOutSummary.nextDueDate);
        const dueDateDiff = dueDate.diff(today, 'days');
        badgeText = Math.abs(dueDateDiff).toString();
        badgeColor = libraryInfo.checkedOutSummary.isExpired ? '#ff3333' : '#3385ff';
      }

      this._lastRefreshTime = new Date().getTime();
      this.chromeService.setBadge(badgeText, badgeColor);

      this.chromeService.writeStorageData({
        lastRefresh: moment().format('YYYY-MM-DD'),
        lastRefreshTime: this._lastRefreshTime,
        checkedOutSummary: libraryInfo.checkedOutSummary,
        checkedOutItems: libraryInfo.checkedOutItems,
        heldItems: libraryInfo.heldItems,
        heldSummary: libraryInfo.heldSummary
      });

      this.checkedOutItems = libraryInfo.checkedOutItems;
      this.heldItems = libraryInfo.heldItems;
      this.isLoading = false;
    });
  }

  public get lastRefreshDate(): string {
    return this._lastRefreshTime && this._lastRefreshTime && moment(this._lastRefreshTime).format('YYYY/MM/DD hh:mm');
  }

  public toggleTab($event): void {
    $event.preventDefault();
    $event.stopPropagation();
    this.activeTabIndex = (this.activeTabIndex === 0 ? 1 : 0);
  }
}
