import { Component, OnInit, Input } from '@angular/core';

import { CheckedOutItem } from '../app.typings';
import * as moment from 'moment';

const today = moment().startOf('day');

@Component({
  selector: 'app-checkedout-list',
  templateUrl: './checkedout-list.component.html',
  styleUrls: ['./checkedout-list.component.less']
})
export class CheckedoutListComponent implements OnInit {
  private _items: CheckedOutItem[] = [];

  constructor() { }

  ngOnInit() {
  }

  @Input()
  public set items(checkedOutItems: CheckedOutItem[]) {
    this._items = this.processItems(checkedOutItems);
  }

  public get items(): CheckedOutItem[] {
    return this._items;
  }

  private processItems(items: CheckedOutItem[]): CheckedOutItem[] {
    items = items.slice();

    items.forEach(item => {
      const dueDate = moment(item.dueDate);
      const dueDateTime = dueDate.toDate().getTime();
      const dueDateDiff = dueDate.diff(today, 'days');

      item.dueDateFormatted = dueDate.format('ll');
      item.daysRemaining = Math.abs(dueDateDiff);
      item.dueDateTime = dueDateTime;
      item.isExpired = dueDateDiff < 0;
    });

    items.sort((a, b) => a.dueDateTime - b.dueDateTime);

    return items;
  }
}
