/*
  - chromeServiceProvider = 'test' | 'extension' | 'web'
  - splFunctionsUrl =
    - 'http://localhost:7071/api/GetSampleLibraryItems'
    - 'https://spl-functions.azurewebsites.net/api/GetSampleLibraryItems'
*/

export const Config = {
  chromeServiceProvider: 'web',
  splFunctionsUrl: 'https://spl-functions.azurewebsites.net/api/GetLibraryStatus'
};
