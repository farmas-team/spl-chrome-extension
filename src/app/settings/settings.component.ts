import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { Settings } from '../app.typings';
import { ChromeService } from '../chrome.service';
import { Config } from '../config';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.less']
})
export class SettingsComponent implements OnInit {
  public hasInitialSettings = false;
  public settings: Settings = {
    dataToken: null,
    dataUrl: Config.splFunctionsUrl,
    splPassword: null,
    splUsername: null
  };

  constructor(private chromeService: ChromeService, private router: Router) { }

  ngOnInit() {
    this.chromeService.readStorageData('settings').subscribe(obj => {
      const settings = obj['settings'];

      this.hasInitialSettings = !!settings;

      if (this.hasInitialSettings) {
        this.settings.dataUrl = settings.dataUrl;
        this.settings.dataToken = settings.dataToken;
        this.settings.splUsername = settings.splUsername;
        this.settings.splPassword = settings.splPassword;
      }
    });
  }

  public save(): void {
    console.log('Saving settings', this.settings);
    this.chromeService.writeStorageData({ settings: this.settings }, () => {
      this.router.navigate(['/']);
    });
  }

  public close(): void {
    this.router.navigate(['/']);
  }
}
