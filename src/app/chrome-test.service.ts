import { Injectable } from '@angular/core';

import { SampleLibraryInfo } from './mock-library-items';
import { ChromeService } from './chrome.service';
import { LocalStorage } from './app.typings';
import { Observable, of } from 'rxjs';

let localStorage: LocalStorage = {
  checkedOutItems: SampleLibraryInfo.checkedOutItems,
  heldItems: SampleLibraryInfo.heldItems,
  lastRefreshTime: new Date().getTime()
};

@Injectable({
  providedIn: 'root'
})
export class ChromeTestService extends ChromeService {

  constructor() {
    super();
  }

  readStorageData(keys: string | string[] | Object | null): Observable<LocalStorage> {
    console.log('ChromeTestService.readStorageData', localStorage);
    return of(localStorage);
  }

  writeStorageData(data: LocalStorage, callback?: () => void): void {
    console.log('ChromeTestService.writeStorageData', data);
    localStorage = { ...localStorage, ...data };

    // tslint:disable-next-line:no-unused-expression
    callback && callback();
  }

  setBadge(text: string, color: string): void {
    // no op.
  }
}
