import { Component, OnInit, Input } from '@angular/core';
import { HeldItem } from '../app.typings';

@Component({
  selector: 'app-held-list',
  templateUrl: './held-list.component.html',
  styleUrls: ['./held-list.component.less']
})
export class HeldListComponent implements OnInit {
  @Input()
  public items: HeldItem[] = [];

  constructor() { }

  ngOnInit() {
  }

}
