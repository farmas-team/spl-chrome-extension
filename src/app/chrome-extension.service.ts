/// <reference types="chrome"/>

import { Injectable } from '@angular/core';
import { ChromeService } from './chrome.service';
import { LocalStorage } from './app.typings';
import { Observable, from } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ChromeExtensionService extends ChromeService {

  constructor() {
    super();
  }

  readStorageData(keys: string | string[] | Object | null): Observable<LocalStorage> {
    const promise = new Promise<LocalStorage>(resolve => {
      chrome.storage.sync.get(keys, (result) => {
        resolve(result);
      });
    });

    return from(promise);
  }

  writeStorageData(data: LocalStorage, callback?: () => void): void {
    console.log('Writting to chrome storage', data);
    chrome.storage.sync.set(data, callback);
  }

  setBadge(text: string, color: string): void {
    console.log('Setting chrome badge', text, color);
    chrome.browserAction.setBadgeText({ text: text });
    chrome.browserAction.setBadgeBackgroundColor({ color: color });
  }
}
