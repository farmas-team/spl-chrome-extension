import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CheckedoutListComponent } from './checkedout-list/checkedout-list.component';
import { ChromeService } from './chrome.service';
import { ChromeServiceFactory } from './chrome.service.factory';
import { SettingsComponent } from './settings/settings.component';
import { MainComponent } from './main/main.component';
import { HeldListComponent } from './held-list/held-list.component';
import { SplService } from './spl.service';
import { ActivatedRoute } from '@angular/router';

@NgModule({
  declarations: [
    AppComponent,
    CheckedoutListComponent,
    SettingsComponent,
    MainComponent,
    HeldListComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [{
    provide: ChromeService,
    useFactory: ChromeServiceFactory,
    deps: [SplService, ActivatedRoute]
  }],
  bootstrap: [AppComponent]
})
export class AppModule { }
