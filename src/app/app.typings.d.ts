export interface Settings {
  dataUrl: string;
  dataToken: string;
  splUsername: string;
  splPassword: string;
}

export interface LocalStorage extends LibraryInfo {
  lastRefresh?: string;
  lastRefreshTime?: number;
}

export interface LibraryInfo {
  settings?: Settings;
  checkedOutItems?: CheckedOutItem[];
  checkedOutSummary?: CheckedOutSummary;
  heldItems?: HeldItem[];
  heldSummary?: HeldSummary;
}

export interface HeldItem {
  metadataId: string;
  title: string;
  imageSrc: string;
  isReady: boolean;
  isInTransit: boolean;
  position: number;
}

export interface HeldSummary {
  total: number;
  anyReady: boolean;
}

export interface CheckedOutSummary {
  nextDueDate: string;
  isExpired: boolean;
}

export interface CheckedOutItem {
  metadataId: string;
  barcode: string;
  title: string;
  imageSrc: string;
  isExpired?: boolean;
  dueDate: string;
  daysRemaining?: number;
  dueDateFormatted?: string;
  dueDateTime?: number;
}
