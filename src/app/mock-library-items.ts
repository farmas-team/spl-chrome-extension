import { LibraryInfo } from './app.typings';

export const SampleLibraryInfo: LibraryInfo = {
  checkedOutItems: [
    {
      'title': 'The Righteous Mind',
      'imageSrc': 'https://secure.syndetics.com/index.aspx?isbn=9780307907035/SC.GIF&client=sepup&type=xw12&oclc=',
      'barcode': 'mq4502898',
      'isExpired': false,
      'dueDate': '2018-12-11',
      'daysRemaining': null,
      'metadataId': 'S30C2819725'
    },
    {
      'title': 'The Big Bang Theory',
      'imageSrc': 'https://secure.syndetics.com/index.aspx?isbn=/SC.GIF&client=sepup&type=xw12&oclc=&upc=883929566648',
      'barcode': '0010090550723',
      'isExpired': false,
      'dueDate': '2018-12-13',
      'daysRemaining': null,
      'metadataId': 'S30C3282119'
    },
    {
      'title': '5-minute Princess Stories',
      'imageSrc': 'https://secure.syndetics.com/index.aspx?isbn=9781423146575/SC.GIF&client=sepup&type=xw12&oclc=',
      'barcode': '0010092139319',
      'isExpired': false,
      'dueDate': '2018-12-12',
      'daysRemaining': null,
      'metadataId': 'S30C2876756'
    },
    {
      'title': 'Atomic Blonde',
      'imageSrc': 'https://secure.syndetics.com/index.aspx?isbn=9786317017986/SC.GIF&client=sepup&type=xw12&oclc=&upc=025192396915',
      'barcode': '0010093117983',
      'isExpired': false,
      'dueDate': '2018-12-14',
      'daysRemaining': null,
      'metadataId': 'S30C3299221'
    },
    {
      'title': 'The Catcher Was A Spy',
      'imageSrc': 'https://secure.syndetics.com/index.aspx?isbn=/SC.GIF&client=sepup&type=xw12&oclc=&upc=032429311979',
      'barcode': '0010096870760',
      'isExpired': false,
      'dueDate': '2018-12-26',
      'daysRemaining': null,
      'metadataId': 'S30C3404311'
    },
    {
      'title': 'Eighth Grade',
      'imageSrc': 'https://secure.syndetics.com/index.aspx?isbn=/SC.GIF&client=sepup&type=xw12&oclc=&upc=031398292487',
      'barcode': '0010096091615',
      'isExpired': false,
      'dueDate': '2018-12-26',
      'daysRemaining': null,
      'metadataId': 'S30C3401880'
    },
    {
      'title': 'The Good Fight',
      'imageSrc': 'https://secure.syndetics.com/index.aspx?isbn=9786317214750/SC.GIF&client=sepup&type=xw12&oclc=&upc=032429305312',
      'barcode': '0010093338167',
      'isExpired': false,
      'dueDate': '2018-12-26',
      'daysRemaining': null,
      'metadataId': 'S30C3363386'
    }
  ],
  'heldItems': [
    {
      'title': 'interview with God',
      'imageSrc': 'https://secure.syndetics.com/index.aspx?isbn=9786317466487/SC.GIF&client=sepup&type=xw12&oclc=&upc=191329078211',
      'metadataId': 'S30C3422748',
      'isReady': true,
      'isInTransit': false,
      'position': 0
    },
    {
      'title': 'Sicario Day of the soldado',
      'imageSrc': 'https://secure.syndetics.com/index.aspx?isbn=/SC.GIF&client=sepup&type=xw12&oclc=&upc=043396526815',
      'metadataId': 'S30C3401911',
      'isReady': false,
      'isInTransit': true,
      'position': 60
    },
    {
      'title': 'iGen',
      'imageSrc': 'https://secure.syndetics.com/index.aspx?isbn=9781501152023/SC.GIF&client=sepup&type=xw12&oclc=',
      'metadataId': 'S30C3296944',
      'isReady': false,
      'isInTransit': false,
      'position': 4
    },
    {
      'title': 'MEMOIR OF WAR DVD',
      'imageSrc': 'https://secure.syndetics.com/index.aspx?isbn=/SC.GIF&client=sepup&type=xw12&oclc=&upc=751778951185',
      'metadataId': 'S30C3422757',
      'isReady': false,
      'isInTransit': false,
      'position': 36
    },
    {
      'title': 'big bang theory The complete eleventh season',
      'imageSrc': 'https://secure.syndetics.com/index.aspx?isbn=/SC.GIF&client=sepup&type=xw12&oclc=&upc=883929609062',
      'metadataId': 'S30C3389119',
      'isReady': false,
      'isInTransit': false,
      'position': 49
    },
    {
      'title': 'CAPTAIN DVD',
      'imageSrc': 'https://secure.syndetics.com/index.aspx?isbn=/SC.GIF&client=sepup&type=xw12&oclc=&upc=751778951208',
      'metadataId': 'S30C3421953',
      'isReady': false,
      'isInTransit': false,
      'position': 63
    },
    {
      'title': 'death of Stalin',
      'imageSrc': 'https://secure.syndetics.com/index.aspx?isbn=/SC.GIF&client=sepup&type=xw12&oclc=&upc=032429308405',
      'metadataId': 'S30C3377975',
      'isReady': false,
      'isInTransit': false,
      'position': 63
    },
    {
      'title': 'Children Act',
      'imageSrc': 'https://secure.syndetics.com/index.aspx?isbn=/SC.GIF&client=sepup&type=xw12&oclc=&upc=031398293972',
      'metadataId': 'S30C3411270',
      'isReady': false,
      'isInTransit': false,
      'position': 69
    },
    {
      'title': 'Leave no trace',
      'imageSrc': 'https://secure.syndetics.com/index.aspx?isbn=/SC.GIF&client=sepup&type=xw12&oclc=&upc=191329065624',
      'metadataId': 'S30C3398077',
      'isReady': false,
      'isInTransit': false,
      'position': 85
    },
    {
      'title': 'good fight Season two',
      'imageSrc': 'https://secure.syndetics.com/index.aspx?isbn=/SC.GIF&client=sepup&type=xw12&oclc=&upc=032429312952',
      'metadataId': 'S30C3405813',
      'isReady': false,
      'isInTransit': false,
      'position': 95
    },
    {
      'title': 'Alpha',
      'imageSrc': 'https://secure.syndetics.com/index.aspx?isbn=/SC.GIF&client=sepup&type=xw12&oclc=&upc=043396499218',
      'metadataId': 'S30C3411264',
      'isReady': false,
      'isInTransit': false,
      'position': 100
    },
    {
      'title': '12th man',
      'imageSrc': 'https://secure.syndetics.com/index.aspx?isbn=/SC.GIF&client=sepup&type=xw12&oclc=&upc=826663189629',
      'metadataId': 'S30C3420207',
      'isReady': false,
      'isInTransit': false,
      'position': 131
    },
    {
      'title': 'Fahrenheit 451',
      'imageSrc': 'https://secure.syndetics.com/index.aspx?isbn=/SC.GIF&client=sepup&type=xw12&oclc=&upc=883929647491',
      'metadataId': 'S30C3407189',
      'isReady': false,
      'isInTransit': false,
      'position': 134
    },
    {
      'title': 'meg',
      'imageSrc': 'https://secure.syndetics.com/index.aspx?isbn=/SC.GIF&client=sepup&type=xw12&oclc=&upc=883929623358',
      'metadataId': 'S30C3411292',
      'isReady': false,
      'isInTransit': false,
      'position': 153
    },
    {
      'title': 'Happytime murders',
      'imageSrc': 'https://secure.syndetics.com/index.aspx?isbn=/SC.GIF&client=sepup&type=xw12&oclc=&upc=191329042069',
      'metadataId': 'S30C3411286',
      'isReady': false,
      'isInTransit': false,
      'position': 170
    },
    {
      'title': 'Searching',
      'imageSrc': 'https://secure.syndetics.com/index.aspx?isbn=/SC.GIF&client=sepup&type=xw12&oclc=&upc=043396542242',
      'metadataId': 'S30C3411298',
      'isReady': false,
      'isInTransit': false,
      'position': 176
    },
    {
      'title': 'catcher was a spy',
      'imageSrc': 'https://secure.syndetics.com/index.aspx?isbn=/SC.GIF&client=sepup&type=xw12&oclc=&upc=032429311979',
      'metadataId': 'S30C3404311',
      'isReady': false,
      'isInTransit': false,
      'position': 190
    },
    {
      'title': 'house with a clock in its walls',
      'imageSrc': 'https://secure.syndetics.com/index.aspx?isbn=/SC.GIF&client=sepup&type=xw12&oclc=&upc=191329068465',
      'metadataId': 'S30C3421966',
      'isReady': false,
      'isInTransit': false,
      'position': 207
    },
    {
      'title': 'Galveston',
      'imageSrc': 'https://secure.syndetics.com/index.aspx?isbn=/SC.GIF&client=sepup&type=xw12&oclc=&upc=014381103694',
      'metadataId': 'S30C3425977',
      'isReady': false,
      'isInTransit': false,
      'position': 210
    },
    {
      'title': 'Sorry to bother you',
      'imageSrc': 'https://secure.syndetics.com/index.aspx?isbn=/SC.GIF&client=sepup&type=xw12&oclc=&upc=024543548997',
      'metadataId': 'S30C3405829',
      'isReady': false,
      'isInTransit': false,
      'position': 224
    },
    {
      'title': 'HUNTER KILLER DVD',
      'imageSrc': 'https://secure.syndetics.com/index.aspx?isbn=/SC.GIF&client=sepup&type=xw12&oclc=&upc=031398298069',
      'metadataId': 'S30C3428830',
      'isReady': false,
      'isInTransit': false,
      'position': 253
    },
    {
      'title': 'predator',
      'imageSrc': 'https://secure.syndetics.com/index.aspx?isbn=9786317504349/SC.GIF&client=sepup&type=xw12&oclc=&upc=024543394228',
      'metadataId': 'S30C3428888',
      'isReady': false,
      'isInTransit': false,
      'position': 255
    },
    {
      'title': 'MID90S DVD',
      'imageSrc': 'https://secure.syndetics.com/index.aspx?isbn=/SC.GIF&client=sepup&type=xw12&oclc=&upc=031398298960',
      'metadataId': 'S30C3426767',
      'isReady': false,
      'isInTransit': false,
      'position': 261
    },
    {
      'title': 'equalizer 2',
      'imageSrc': 'https://secure.syndetics.com/index.aspx?isbn=/SC.GIF&client=sepup&type=xw12&oclc=&upc=043396488175',
      'metadataId': 'S30C3411282',
      'isReady': false,
      'isInTransit': false,
      'position': 265
    },
    {
      'title': 'NIGHT SCHOOL DVD',
      'imageSrc': 'https://secure.syndetics.com/index.aspx?isbn=/SC.GIF&client=sepup&type=xw12&oclc=&upc=191329041444',
      'metadataId': 'S30C3426769',
      'isReady': false,
      'isInTransit': false,
      'position': 270
    }
  ]
};
