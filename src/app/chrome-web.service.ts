import { Injectable } from '@angular/core';
import { SplService } from './spl.service';
import { ChromeService } from './chrome.service';
import { Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';
import { LocalStorage, Settings } from './app.typings';
import { Config } from './config';
import { ActivatedRoute } from '@angular/router';
import * as  moment from 'moment';

const storageKey = 'spl-data';

const anonymousSettings: Settings = {
  splUsername: null,
  splPassword: null,
  dataUrl: Config.splFunctionsUrl,
  dataToken: null
};

@Injectable({
  providedIn: 'root'
})
export class ChromeWebService extends ChromeService {

  constructor(private splService: SplService, private route: ActivatedRoute) {
    super();
  }

  readStorageData(keys: string | string[] | Object | null): Observable<LocalStorage> {
    const today = moment().startOf('hour');
    const splData: LocalStorage = JSON.parse(window.localStorage.getItem(storageKey));
    const lastRefreshDate = splData && moment(splData.lastRefreshTime);
    const diff = lastRefreshDate && Math.abs(lastRefreshDate.diff(today, 'hour'));
    const force = this.route.snapshot.queryParamMap.has('force');

    if (!force && diff != null && diff < 1) {
      console.log('Less than an hour since last refresh', lastRefreshDate.toString());
      return of(splData);
    } else {
      anonymousSettings.dataToken = this.route.snapshot.queryParamMap.get('key');

      return this.splService.getLibraryItems(anonymousSettings).pipe(
        map((data: LocalStorage) => {
          data.settings = anonymousSettings;
          data.lastRefreshTime = new Date().getTime();
          this.writeStorageData(data);

          return data;
        })
      );
    }
  }

  writeStorageData(data: LocalStorage, callback?: () => void): void {
    data.settings = anonymousSettings;
    data.lastRefreshTime = new Date().getTime();
    window.localStorage.setItem(storageKey, JSON.stringify(data));
  }

  setBadge(text: string, color: string): void {
    // no op.
  }
}
