# Seattle Public Library Chrome Extension

This chrome extension uses an Azure back end to query the status of your items from the Seattle Public Library website.

## Download

You can download and install the extension from the chrome webstore [here](https://chrome.google.com/webstore/detail/seattle-public-library-st/mcnbnnlfhnckmklfjaiobhmdekhijfja).

## Setup

### 1. Prepare back end
This extension needs a special back-end that queries the Seattle Public Library on your behalf. The back end is developed as an Azure Function and needs to be deployed prior to use this extension.

Instructions of how to setup the back-end are [here](https://bitbucket.org/farmas-team/spl-functions).

Note: Multiple users can use the same back end.

### 2. Configure the extension

1. On first load, the extension will show the settings screen. 
2. Set the Data Url to the url for GetLibraryStatus azure function.
3. Set your Seattle Public Library user and password.
4. Click 'Save' and close the settings screen.

## Attributions

Icon made by [Smashicons](https://www.flaticon.com/authors/smashicons) from [www.flaticon.com](https://www.flaticon.com) is licensed by [CC 3.0 BY](http://creativecommons.org/licenses/by/3.0)

## Local Development

- Install Angular 7 CLI.
- Run 'npm install' from root.
- Make sure to start the local development azure function host as described in the functions project.
- Open Config.ts and change the 'ChromeServiceProvider' to 'test'.
- Run 'ng serve --open' from root.

## Run in Chrome

- Run 'ng build' from root.
- Load the distribution folder as an unpacked extension into Chrome browser.


